import time
import requests
from datetime import datetime


def live_checker(streamer_name):
    twitch_api_stream_url = 'https://api.twitch.tv/kraken/streams/' \
                            + streamer_name + '?client_id=' + client_id

    streamer_html = requests.get(twitch_api_stream_url)
    result = streamer_html.text

    if '"stream_type":"live"' in result:
        if counter == 0:
            first_positive_update(streamer_name)
        else:
            positive_update(streamer_name)
    else:
        negative_update(streamer_name)


def first_positive_update(streamer_name):
    f = open('streamers_live.json', 'a')
    f.write('{"userId":"' + streamer_name + ',"status":"live"}')
    f.close()
    global counter
    counter = counter + 1


def positive_update(streamer_name):
    f = open('streamers_live.json', 'a')
    f.write(',{"userId":"' + streamer_name + '","status":"live"}')
    f.close()


def negative_update(streamer_name):
    f = open('streamers_offline.txt', 'a')
    f.write('\n' + streamer_name)
    f.close()


def file_create():
    f = open('streamers_live.json', 'w')
    f.write('{"Accounts":[')
    f.close()
    time_unformatted = time.time()
    timestamp = datetime.fromtimestamp(time_unformatted).strftime('%Y-%m-%d %H:%M:%S')
    f = open('streamers_offline.txt', 'w')
    f.write(timestamp)
    f.close()


def file_close():
    f = open('streamers_live.json', 'a')
    f.write(']}')
    f.close()


while True:  # Makes it so the code never ends.
    file_create()
    client_id = 'inset_client_id from https://dev.twitch.tv/console'
    counter = 0
    # live_checker('channel_name')
    # live_checker('channel_name')
    file_close()
    time.sleep(60)  # Waits 60 seconds ( executes code again )
